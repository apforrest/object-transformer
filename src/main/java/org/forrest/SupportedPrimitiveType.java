package org.forrest;

import java.util.Optional;

enum SupportedPrimitiveType {

    INT(Integer.class, int.class),
    DOUBLE(Double.class, double.class),
    LONG(Long.class, long.class),
    BOOLEAN(Boolean.class, boolean.class);

    private final Class<?> objectType;
    private final Class<?> primitiveType;

    SupportedPrimitiveType(Class<?> objectType, Class<?> primitiveType) {
        this.objectType = objectType;
        this.primitiveType = primitiveType;
    }

    boolean isAssignableFrom(Class<?> otherType) {
        return objectType.isAssignableFrom(otherType) || primitiveType.isAssignableFrom(otherType);
    }

    static Optional<SupportedPrimitiveType> find(Class<?> otherType) {
        for (SupportedPrimitiveType primitive : values()) {
            if (primitive.isAssignableFrom(otherType)) {
                return Optional.of(primitive);
            }
        }
        return Optional.empty();
    }

}
