package org.forrest;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.Collection;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@Inherited
@Documented
public @interface AttributeKey {

    String value();

    Class<? extends ValueMapper<Collection<String>, ?>> mapper() default StringCollectionToBasicTypes.class;

    boolean required() default true;

    String[] defaultValues() default {};

}