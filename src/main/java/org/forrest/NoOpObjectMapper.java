package org.forrest;

public final class NoOpObjectMapper implements ValueMapper<Object, Object> {

    @Override
    public Object map(Object value, Class<?> fieldType) {
        return value;
    }

}
