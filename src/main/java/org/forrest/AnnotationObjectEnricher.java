package org.forrest;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;

public final class AnnotationObjectEnricher implements ObjectEnricher {

    private final SegmentDescriptor descriptor;
    private final Function<Object, Object> inspector;

    public AnnotationObjectEnricher(SegmentDescriptor descriptor, Function<Object, Object> inspector) {
        this.descriptor = descriptor;
        this.inspector = inspector;
    }

    public AnnotationObjectEnricher() {
        this(new DashSeparatedDescriptor());
    }

    public AnnotationObjectEnricher(SegmentDescriptor descriptor) {
        this(descriptor, (object) -> object);
    }

    public AnnotationObjectEnricher(Function<Object, Object> inspector) {
        this(new DashSeparatedDescriptor(), inspector);
    }

    @Override
    public <T, S> void enrich(T target, S source) {
        Object inspectedSource = inspector.apply(source);
        inspect(target, (field) -> inspectAndSetField(field, inspectedSource, target));

    }

    @Override
    public <T> void enrich(T target, Map<String, ? extends Collection<String>> keyValues) {
        inspect(target, (field) -> inspectAndSetField(field, keyValues, target));
    }

    @Override
    public <S, T> void replenish(S staleSource, T latestTarget) {
        throw new RuntimeException("Not implemented");
    }

    private void inspect(Object target, Consumer<Field> fieldConsumer) {
        for (Field field : collectFields(target.getClass())) {
            if (Modifier.isPrivate(field.getModifiers())) {
                field.setAccessible(true);
            }
            fieldConsumer.accept(field);
        }
    }

    private List<Field> collectFields(Class<?> targetClass) {
        List<Field> fields = Arrays.asList(targetClass.getDeclaredFields());
        if (targetClass.getSuperclass() != null) {
            fields.addAll(collectFields(targetClass.getSuperclass()));
        }
        return fields;
    }

    private void inspectAndSetField(Field field, Object source, Object target) {
        if (field.isAnnotationPresent(AttributeCollection.class)) {
            AttributeCollection attributeCollection = field.getAnnotation(AttributeCollection.class);
            retrieveAndPopulate(attributeCollection, field, source, target);
        } else if (field.isAnnotationPresent(AttributePath.class)) {
            AttributePath attributePath = field.getAnnotation(AttributePath.class);
            retrieveAndPopulate(attributePath, field, source, target);
        } else if (field.isAnnotationPresent(NestedObject.class)) {
            instantiateNestedObject(field, source, target);
        }
    }

    private void inspectAndSetField(Field field, Map<String, ? extends Collection<String>> keyValues, Object target) {
        if (field.isAnnotationPresent(AttributeKey.class)) {
            AttributeKey attribute = field.getAnnotation(AttributeKey.class);
            retrieveAndPopulate(attribute, field, keyValues, target);
        }
    }

    private void retrieveAndPopulate(AttributePath attributePath, Field field, Object source, Object target) {
        List<String> segments = splitPathIntoSegments(attributePath.value());

        if (segments.isEmpty()) {
            throw new IllegalArgumentException("Invalid attribute path, no segments");
        }

        Class<?> fieldType = field.getType();

        SegmentDescriptor descriptor = attributePath.descriptor().equals(this.descriptor.getClass())
                ? this.descriptor : createInstance(attributePath.descriptor());
        Object value = traverseObjectGraph(source, segments, descriptor);

        ValueMapper<Object, ?> mapper = createInstance(attributePath.mapper());
        setField(field, invokeTypeSafeMapping(mapper, value, fieldType), target);
    }

    private void instantiateNestedObject(Field field, Object source, Object target) {
        Class<?> nestedObjectType = field.getType();
        Object nestedObject = instantiateAndPopulate(nestedObjectType, source);
        setField(field, nestedObject, target);
    }

    private void retrieveAndPopulate(AttributeCollection attributeCollection,
            Field field, Object source, Object target) {

        if (!List.class.isAssignableFrom(field.getType())) {
            throw new IllegalArgumentException("Target value is not a list");
        }

        Type[] genericListTypes = ((ParameterizedType) field.getGenericType()).getActualTypeArguments();

        if (genericListTypes.length > 1) {
            throw new IllegalArgumentException("Target list type must be of a single type");
        }

        Class<?> listType = (Class) genericListTypes[0];
        List<String> segments = splitPathIntoSegments(attributeCollection.value());

        if (segments.isEmpty()) {
            throw new IllegalArgumentException("Invalid attribute path, no segments");
        }

        Object value = traverseObjectGraph(source, segments, descriptor);

        if (!Collection.class.isAssignableFrom(value.getClass())) {
            throw new IllegalArgumentException("Source value is not a collection");
        }

        Collection<?> sourceCollection = (Collection<?>) value;
        List<Object> targetList = new ArrayList<>(sourceCollection.size());

        for (Object sourceEntry : sourceCollection) {
            targetList.add(instantiateAndPopulate(listType, sourceEntry));
        }

        setField(field, targetList, target);
    }

    private void retrieveAndPopulate(AttributeKey keyValueAttribute, Field field,
            Map<String, ? extends Collection<String>> keyValues, Object target) {
        Class<?> fieldType = field.getType();

        String attributeKey = keyValueAttribute.value();
        Collection<String> values = keyValues.get(attributeKey);

        if (values == null || values.isEmpty()) {
            if (keyValueAttribute.required()) {
                throw new IllegalArgumentException("Required attribute " + attributeKey);
            }

            if (keyValueAttribute.defaultValues().length == 0) {
                return;
            }

            values = new HashSet<>(Arrays.asList(keyValueAttribute.defaultValues()));
        }

        ValueMapper<Collection<String>, ?> mapper = createInstance(keyValueAttribute.mapper());
        setField(field, invokeTypeSafeMapping(mapper, values, fieldType), target);
    }

    private <V, F> F invokeTypeSafeMapping(ValueMapper<V, F> mapper, V value, Class<?> fieldType) {
        // Source field type must equate to the return value type of the mapper
        @SuppressWarnings("unchecked")
        Class<F> typedFieldClass = (Class<F>) fieldType;
        F mappedValue = mapper.map(value, typedFieldClass);

        if (!fieldType.isAssignableFrom(mappedValue.getClass())) {
            boolean primitiveTypeMatch = SupportedPrimitiveType.find(fieldType)
                    .map(primitiveType -> primitiveType.isAssignableFrom(mappedValue.getClass()))
                    .orElse(Boolean.FALSE);

            if (!primitiveTypeMatch) {
                throw new IllegalStateException("The mapped value does not correspond to the expected field type");
            }
        }

        return mappedValue;
    }

    private Object traverseObjectGraph(Object source, List<String> segments, SegmentDescriptor descriptor) {
        return traverseObjectGraph(source, segments, descriptor, 0);
    }

    private Object traverseObjectGraph(Object source, List<String> segments, SegmentDescriptor descriptor, int segmentIndex) {
        try {
            SegmentDescriptor.Position position = (segmentIndex + 1 == segments.size())
                    ? SegmentDescriptor.Position.LEAF
                    : SegmentDescriptor.Position.PATH;

            String sourceFieldName = descriptor.toFieldDefinition(segments.get(segmentIndex), position);
            Field sourceField = getField(sourceFieldName, source.getClass());

            if (Modifier.isPrivate(sourceField.getModifiers())) {
                sourceField.setAccessible(true);
            }

            String methodPrefix = Boolean.class.isAssignableFrom(sourceField.getType()) ? "is" : "get";
            String methodName = methodPrefix + Character.toUpperCase(sourceFieldName.charAt(0))
                    + sourceFieldName.substring(1);
            Method sourceGetterMethod = source.getClass().getMethod(methodName);
            Object sourceValue = sourceGetterMethod.invoke(source);

            if (segmentIndex + 1 == segments.size()) {
                // last segment
                return sourceValue;
            }

            if (Collection.class.isAssignableFrom(sourceValue.getClass())) {
                Collection<?> sourceCollection = (Collection) sourceValue;
                List<Object> targetList = new ArrayList<>(sourceCollection.size());

                for (Object sourceEntry : sourceCollection) {
                    // next segment
                    targetList.add(traverseObjectGraph(inspector.apply(sourceEntry),
                            segments, descriptor, segmentIndex + 1));
                }

                return targetList;
            }

            return traverseObjectGraph(inspector.apply(sourceValue), segments, descriptor, segmentIndex + 1);
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException | NoSuchFieldException e) {
            throw new RuntimeException("Failed to traverse object graph", e);
        }
    }

    private Object instantiateAndPopulate(Class<?> targetType, Object source) {
        Object target = createInstance(targetType);
        enrich(target, inspector.apply(source));
        return target;
    }

    private Field getField(String fieldName, Class<?> object) throws NoSuchFieldException {
        Optional<Field> field = Arrays.stream(object.getDeclaredFields())
                .filter(f -> f.getName().equals(fieldName))
                .findFirst();

        if (field.isPresent()) {
            return field.get();
        }

        if (object.getSuperclass() != null) {
            return getField(fieldName, object.getSuperclass());
        }

        throw new NoSuchFieldException(fieldName);
    }

    private List<String> splitPathIntoSegments(String path) {
        return Arrays.stream(path.split("/"))
                .filter(segment -> !segment.isEmpty())
                .collect(Collectors.toList());
    }

    private <T> T createInstance(Class<T> type) {
        try {
            return type.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            throw new RuntimeException("Failed to instantiate instance of " + type, e);
        }
    }

    private void setField(Field field, Object value, Object target) {
        try {
            field.set(target, value);
        } catch (IllegalAccessException e) {
            throw new RuntimeException("Failed to set value against field " + field, e);
        }
    }

}
