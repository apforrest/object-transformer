package org.forrest;

public final class DashSeparatedDescriptor implements SegmentDescriptor {

    @Override
    public String toFieldDefinition(String segment, Position position) {
        StringBuilder fieldDefinition = new StringBuilder(segment);

        int index;
        while ((index = fieldDefinition.indexOf("-")) > -1) {
            if (index + 1 < fieldDefinition.length()) {
                char upperCaseChar = Character.toUpperCase(fieldDefinition.charAt(index + 1));
                fieldDefinition.setCharAt(index + 1, upperCaseChar);
            }

            fieldDefinition.deleteCharAt(index);
        }

        return fieldDefinition.toString();
    }

}
