/*
 * Copyright 2015-2017 ForgeRock AS. All Rights Reserved
 *
 * Use of this code requires a commercial software license with ForgeRock AS.
 * or with one of its affiliates. All use shall be exclusively subject
 * to such license between the licensee and ForgeRock AS.
 */

package org.forrest;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public final class StringCollectionToBasicTypes implements ValueMapper<Collection<String>, Object> {

    @Override
    public Object map(Collection<String> values, Class<?> fieldType) {
        if (fieldType == Set.class) {
            // Making the assumption that the type is Set<String>.
            return new HashSet<>(values);
        }
        if (fieldType == List.class) {
            // Making the assumption that the type is List<String>.
            return new ArrayList<>(values);
        }

        String value = values.iterator().next();

        if (fieldType == String.class) {
            return value;
        }
        if (SupportedPrimitiveType.BOOLEAN.isAssignableFrom(fieldType)) {
            return Boolean.parseBoolean(value);
        }
        if (SupportedPrimitiveType.INT.isAssignableFrom(fieldType)) {
            return Integer.parseInt(value);
        }
        if (SupportedPrimitiveType.LONG.isAssignableFrom(fieldType)) {
            return Long.parseLong(value);
        }
        if (SupportedPrimitiveType.DOUBLE.isAssignableFrom(fieldType)) {
            return Double.parseDouble(value);
        }

        throw new IllegalArgumentException("Unsupported parameter type " + fieldType);
    }

}
