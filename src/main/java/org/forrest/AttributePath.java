package org.forrest;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@Inherited
@Documented
public @interface AttributePath {

    String value();

    Class<? extends ValueMapper<Object, ?>> mapper() default NoOpObjectMapper.class;

    Class<? extends SegmentDescriptor> descriptor() default DashSeparatedDescriptor.class;

}