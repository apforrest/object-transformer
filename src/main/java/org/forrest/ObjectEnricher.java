package org.forrest;

import java.util.Collection;
import java.util.Map;

public interface ObjectEnricher {

    <T, S> void enrich(T target, S source);

    <T> void enrich(T target, Map<String, ? extends Collection<String>> keyValues);

    <S, T> void replenish(S staleSource, T target);

}
