package org.forrest;

@FunctionalInterface
public interface SegmentDescriptor {

    String toFieldDefinition(String segment, Position position);

    enum Position {
        PATH, LEAF
    }

}
