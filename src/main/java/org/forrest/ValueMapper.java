package org.forrest;

public interface ValueMapper<T, F> {

    F map(T value, Class<? extends F> fieldType);

}
