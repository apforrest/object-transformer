package org.forrest.to;


import java.util.List;
import java.util.stream.Collectors;

import org.forrest.ValueMapper;

public class SquashLists implements ValueMapper<Object, List<String>> {

    @Override
    public List<String> map(Object value, Class<? extends List<String>> fieldType) {
        // Used where type is known to be a nested list of strings.
        @SuppressWarnings("unchecked")
        List<List<String>> values = (List<List<String>>) value;
        return values.stream().flatMap(List::stream).collect(Collectors.toList());
    }

}
