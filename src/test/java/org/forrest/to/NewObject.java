package org.forrest.to;

import java.util.List;

import org.forrest.AttributePath;
import org.forrest.NestedObject;
import org.forrest.AttributeKey;

public class NewObject {

    @NestedObject
    private NewNestedObject newNestedObject;

    @AttributePath(value = "/nested-objects/double-nested-object/values", mapper = SquashLists.class)
    private List<String> numbers;

    @AttributeKey("isAdmin")
    private boolean admin;

    @AttributePath("/name")
    private String id;

    @AttributePath("/nested-object/count")
    private int count;

    public String getId() {
        return id;
    }

    public int getCount() {
        return count;
    }

    public NewNestedObject getNewNestedObject() {
        return newNestedObject;
    }

    public List<String> getNumbers() {
        return numbers;
    }

    public boolean isAdmin() {
        return admin;
    }

}
