package org.forrest.to;

import java.util.List;

import org.forrest.AttributePath;

public class NewDoubleNestedObject {

    @AttributePath("/nested-object/double-nested-object/values")
    private List<String> entries;

    public List<String> getEntries() {
        return entries;
    }

}
