package org.forrest.to;

import org.forrest.AttributePath;
import org.forrest.NestedObject;

public class NewNestedObject {

    @NestedObject
    private NewDoubleNestedObject newDoubleNestedObject;

    @AttributePath("/name")
    private String text;

    public String getText() {
        return text;
    }

    public NewDoubleNestedObject getNewDoubleNestedObject() {
        return newDoubleNestedObject;
    }

}
