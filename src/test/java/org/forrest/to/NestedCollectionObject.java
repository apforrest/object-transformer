package org.forrest.to;

import java.util.List;

import org.forrest.AttributePath;
import org.forrest.AttributeCollection;

public final class NestedCollectionObject {

    @AttributeCollection("/nested-objects")
    private List<NestedObject> nestedObjects;

    public List<NestedObject> getNestedObjects() {
        return nestedObjects;
    }

    public static final class NestedObject {

        @AttributePath("/count")
        private int value;

        public int getValue() {
            return value;
        }

    }

}
