package org.forrest.from;

import java.util.List;

public class SampleObject {

    private List<NestedObject> nestedObjects;
    private NestedObject nestedObject;
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public NestedObject getNestedObject() {
        return nestedObject;
    }

    public void setNestedObject(NestedObject nestedObject) {
        this.nestedObject = nestedObject;
    }

    public List<NestedObject> getNestedObjects() {
        return nestedObjects;
    }

    public void setNestedObjects(List<NestedObject> nestedObjects) {
        this.nestedObjects = nestedObjects;
    }
}
