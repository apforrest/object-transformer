package org.forrest.from;

import java.util.List;

public class DoubleNestedObject {

    private List<String> values;

    public List<String> getValues() {
        return values;
    }

    public void setValues(List<String> values) {
        this.values = values;
    }
}
