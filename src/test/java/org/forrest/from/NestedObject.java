package org.forrest.from;

public class NestedObject {

    private DoubleNestedObject doubleNestedObject;
    private int count;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public DoubleNestedObject getDoubleNestedObject() {
        return doubleNestedObject;
    }

    public void setDoubleNestedObject(DoubleNestedObject doubleNestedObject) {
        this.doubleNestedObject = doubleNestedObject;
    }

}
