package org.forrest;

import static java.util.Collections.singleton;
import static java.util.Collections.singletonMap;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.Arrays;
import java.util.Map;
import java.util.Set;

import org.forrest.from.DoubleNestedObject;
import org.forrest.from.NestedObject;
import org.forrest.from.SampleObject;
import org.forrest.to.NestedCollectionObject;
import org.forrest.to.NewObject;
import org.junit.Before;
import org.junit.Test;

public class TransformTest {

    private ObjectEnricher enricher;

    private SampleObject sampleObject;
    private NestedObject nestedObject;

    @Before
    public void setUp() {
        DoubleNestedObject doubleNestedObject1 = new DoubleNestedObject();
        doubleNestedObject1.setValues(Arrays.asList("1", "2", "3"));
        DoubleNestedObject doubleNestedObject2 = new DoubleNestedObject();
        doubleNestedObject2.setValues(Arrays.asList("0", "9", "8"));

        NestedObject nestedObject1 = new NestedObject();
        nestedObject1.setCount(456);
        nestedObject1.setDoubleNestedObject(doubleNestedObject1);

        NestedObject nestedObject2 = new NestedObject();
        nestedObject2.setCount(789);
        nestedObject2.setDoubleNestedObject(doubleNestedObject2);


        DoubleNestedObject doubleNestedObject = new DoubleNestedObject();
        doubleNestedObject.setValues(Arrays.asList("abc", "def", "hij"));

        nestedObject = new NestedObject();
        nestedObject.setCount(123);
        nestedObject.setDoubleNestedObject(doubleNestedObject);

        sampleObject = new SampleObject();
        sampleObject.setNestedObject(nestedObject);
        sampleObject.setNestedObjects(Arrays.asList(nestedObject1, nestedObject2));
        sampleObject.setName("abc");

        enricher = new AnnotationObjectEnricher();
    }


    @Test
    public void test() {
        NewObject newObject = new NewObject();
        enricher.enrich(newObject, sampleObject);

        assertThat(newObject.getId()).isEqualTo(sampleObject.getName());
        assertThat(newObject.getCount()).isEqualTo(nestedObject.getCount());
        assertThat(newObject.getNewNestedObject()).isNotNull();
        assertThat(newObject.getNewNestedObject().getText()).isEqualTo(sampleObject.getName());
        assertThat(newObject.getNewNestedObject().getNewDoubleNestedObject()).isNotNull();
        assertThat(newObject.getNewNestedObject().getNewDoubleNestedObject().getEntries()).isSubsetOf("abc", "def", "hij");
    }

    @Test
    public void exerciseNestedCollection() {
        NestedCollectionObject testObject = new NestedCollectionObject();
        enricher.enrich(testObject, sampleObject);

        assertThat(testObject.getNestedObjects()).isNotNull();
    }

    @Test
    public void whenPassingKeyValueMapPojoIsPopulated() {
        Map<String, Set<String>> keyValues = singletonMap("isAdmin", singleton("true"));

        NewObject newObject = new NewObject();
        enricher.enrich(newObject, keyValues);

        assertThat(newObject.isAdmin()).isTrue();
    }

}
